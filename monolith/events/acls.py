from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state
    }

    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:

        return {"picture_url": content["photos"][0]["src"]["original"]}

    except (KeyError, IndexError):
        return {"picture_url": None}



# def get_photo(city,state):
#     url = f"https://api.pexels.com/v1/search?query={city}/{state}&per_page=1"
#     headers = {"Authorization": PEXELS_API_KEY}
#     response = requests.get(url, headers=headers)
#     pic = json.loads(response.content)

#     photo = {
#         "picture_url": pic["photos"][0]["src"]["portrait"]
#     }
#     return photo



def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state + "," + "US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": content[0]["lat"],
        "lon": content[0]["lon"],
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        weather_data = {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"]
        }
        return weather_data
    except (KeyError, IndexError):
        return {"weather": None}
